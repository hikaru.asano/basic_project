//LEDが出力されるピンの番号
const int LED_PIN = 13;

#include "HUSKYLENS.h"
#include "SoftwareSerial.h"
#include <Servo.h>

// サーボ設定
Servo servo1;
Servo servo2;
Servo servo3;
const int maxUs = 1900;
const int minUs = 1100;
const int servo1Pin = 10; //表記は11前
const int servo1Period = 50;
const int servo2Pin = 11; //表記は12右
const int servo2Period = 50;
const int servo3Pin = 9; //表記は13左
const int servo3Period = 50;

int servo1Us = 1500;
int servo2Us = 1500;
int servo3Us = 1500;

int straight_x = 1800;
int straight_y = 1800;
int straight_z = 1800;

int right_x = 1800;
int right_y = 1400;
int right_z = 1800;

int left_x = 1800;
int left_y = 1800;
int left_z = 1600;

int right_small_x = 1400;
int right_small_y = 1300;
int right_small_z = 1400;

int left_small_x = 1400;
int left_small_y = 1400;
int left_small_z = 1300;

int stay_count = 0;

HUSKYLENS huskylens;
SoftwareSerial mySerial(3, 2); // RX, TX
//HUSKYLENS green line >> Pin 10; blue line >> Pin 11
void printResult(HUSKYLENSResult result);

void setup()
{
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(115200);
  mySerial.begin(9600);
  //servo1.setPeriodHertz(servo1Period);
  servo1.attach(servo1Pin, minUs, maxUs);

  //servo2.setPeriodHertz(servo2Period);
  servo2.attach(servo2Pin, minUs, maxUs);

  //servo3.setPeriodHertz(servo3Period);
  servo3.attach(servo3Pin, minUs, maxUs);
  delay(3000);
  //delay(120000);

  servo1.writeMicroseconds(1500);
  servo2.writeMicroseconds(1500);
  servo3.writeMicroseconds(1500);
  delay(3000);
  while (!huskylens.begin(mySerial))
  {
    Serial.println(F("Begin failed!"));
    Serial.println(F("1.Please recheck the \"Protocol Type\" in HUSKYLENS (General Settings>>Protocol Type>>Serial 9600)"));
    Serial.println(F("2.Please recheck the connection."));
    delay(100);
  }
  //servo_setup();
}
void servo_setup()
{
    delay(100);
  // Debug console
  //Serial.begin(9600);


  servo1.attach(servo1Pin, minUs, maxUs);
  servo2.attach(servo2Pin, minUs, maxUs);
  servo3.attach(servo3Pin, minUs, maxUs);

   delay(3000);

  servo1.writeMicroseconds(1500);
  delay(1000);

  servo2.writeMicroseconds(1500);
delay(1000);

  servo3.writeMicroseconds(1500);
    delay(1000);

}
//  ここからが，メインのコード，上はおまじないみたいなもの
void loop()
{
  //  もし，リクエストが拒否された場合（うまく接続されなかった場合）
  if (!huskylens.request())
    Serial.println(F("Fail to request data from HUSKYLENS, recheck the connection!"));
  // もし，ハスキーレンズが物体の学習を行っていない場合
  else if (!huskylens.isLearned())
    Serial.println(F("Nothing learned, press learn button on HUSKYLENS to learn one!"));
  // もし，ハスキーレンズがavailableでない場合（なんかエラー出たとき）
  else if (!huskylens.available())
  {
    Serial.println(F("対象画像が見えないので右回転"));
    control_servo(right_x, right_y, right_z);
    stay_count += 1;
    if (stay_count >= 3)
    {
      control_servo(straight_x, straight_y, straight_z);
    }
  }
  // 正常に動いた場合，ここが実際にどうしたいかっていうことを書いている
  else
  {
    stay_count = 0;
    Serial.println(F("###########"));
    while (huskylens.available())
    {
      HUSKYLENSResult result = huskylens.read();
      //  printResultは定義した関数，下に定義がある．
      //int* power;
      //power = calc_servo_power(result);

      servo1Us = calc_servo_power_x(result);
      servo2Us = calc_servo_power_y(result);
      servo3Us = calc_servo_power_z(result);

      Serial.println(servo1Us);
      Serial.println(servo2Us);
      Serial.println(servo3Us);
      control_servo(servo1Us, servo2Us, servo3Us);
      delay(2000);
    }
  }
}

int calc_servo_power_x(HUSKYLENSResult result)
{
  if (result.command == COMMAND_RETURN_BLOCK)
  {
    ///TODO サーボの対応関係が明らかになっていない＋出力について明らかになっていないので，調整が必要
    ///物体の大きさを認識して，残りの距離を推定とかはしたいよね
    ///距離の推定によって，出力を変えるとかは必要だと思うので，それができると非常に嬉しい
    int theta = result.xCenter; //ここで，ターゲットのx軸方向のズレを認識
    theta = theta - 160;        //中心からのズレを計算
    Serial.println(theta);
    if (theta > 20)
    {
      //大きく右にずれているので旋回
      return right_x;
    }
    else if (20 > theta && theta > 5)  
    {
      //右にずれているが，ズレは小さいので，進みながら旋回
      return right_small_x;
    }
    else if (5 > theta && theta > -5)  
    {
      //ズレが小さいので，直進
      return straight_x;
    }
    else if (-5 > theta && theta > -20)  
    {
      //左にずれているが，ズレは小さいので，進みながら旋回
      return left_small_x;
    }
    else
    { //大きく左にずれているので旋回
      return left_x;
    }
  }
}

int calc_servo_power_y(HUSKYLENSResult result)
{
  if (result.command == COMMAND_RETURN_BLOCK)
  {
    ///TODO サーボの対応関係が明らかになっていない＋出力について明らかになっていないので，調整が必要
    ///物体の大きさを認識して，残りの距離を推定とかはしたいよね
    ///距離の推定によって，出力を変えるとかは必要だと思うので，それができると非常に嬉しい
    int theta = result.xCenter; //ここで，ターゲットのx軸方向のズレを認識
    theta = theta - 160;        //中心からのズレを計算
    Serial.println(theta);
    if (theta > 20)
    {
      //大きく右にずれているので旋回
      return right_y;
    }
    else if (20 > theta && theta > 5)
    {
      //右にずれているが，ズレは小さいので，進みながら旋回
      return right_small_y;
    }
    else if (5 > theta && theta > -5)
    {
      //ズレが小さいので，直進
      return straight_y;
    }
    else if (-5 > theta && theta > -20)
    {
      //左にずれているが，ズレは小さいので，進みながら旋回
      return left_small_y;
    }
    else
    {
      //大きく左にずれているので旋回
      return left_y;
    }
  }
}

int calc_servo_power_z(HUSKYLENSResult result)
{
  if (result.command == COMMAND_RETURN_BLOCK)
  {
    ///TODO サーボの対応関係が明らかになっていない＋出力について明らかになっていないので，調整が必要
    ///物体の大きさを認識して，残りの距離を推定とかはしたいよね
    ///距離の推定によって，出力を変えるとかは必要だと思うので，それができると非常に嬉しい
    int theta = result.xCenter; //ここで，ターゲットのx軸方向のズレを認識
    theta = theta - 160;        //中心からのズレを計算
    Serial.println(theta);
    if (theta > 20)
    {
      //大きく右にずれているので旋回
      return right_z;
    }
    else if (20 > theta && theta > 5)
    {
      //右にずれているが，ズレは小さいので，進みながら旋回
      return right_small_z;
    }
    else if (5 > theta && theta > -5)
    {
      //ズレが小さいので，直進
      return straight_z;
    }
    else if (-5 > theta && theta > -20)
    {
      //左にずれているが，ズレは小さいので，進みながら旋回
      return left_small_z;
    }
    else
    {
      //大きく左にずれているので旋回
      return left_z;
    }
  }
}

//サーボ制御

void control_servo(int servo1ms, int servo2ms, int servo3ms)
{
  servo1.writeMicroseconds(servo1ms);
  servo2.writeMicroseconds(servo2ms);
  servo3.writeMicroseconds(servo3ms);
  delay(3000);
}
