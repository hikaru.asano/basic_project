//LEDが出力されるピンの番号
const int LED_PIN = 13;

#include "HUSKYLENS.h"
#include "SoftwareSerial.h"

HUSKYLENS huskylens;
SoftwareSerial mySerial(10, 11); // RX, TX
//HUSKYLENS green line >> Pin 10; blue line >> Pin 11
void printResult(HUSKYLENSResult result);

void setup() {
    pinMode( LED_PIN, OUTPUT );
    Serial.begin(115200);
    mySerial.begin(9600);
    while (!huskylens.begin(mySerial))
    {
        Serial.println(F("Begin failed!"));
        Serial.println(F("1.Please recheck the \"Protocol Type\" in HUSKYLENS (General Settings>>Protocol Type>>Serial 9600)"));
        Serial.println(F("2.Please recheck the connection."));
        delay(100);
    }
}
//　ここからが，メインのコード，上はおまじないみたいなもの
void loop() {
    //　もし，リクエストが拒否された場合（うまく接続されなかった場合）
    if (!huskylens.request()) Serial.println(F("Fail to request data from HUSKYLENS, recheck the connection!"));
    // もし，ハスキーレンズが物体の学習を行っていない場合
    else if(!huskylens.isLearned()) Serial.println(F("Nothing learned, press learn button on HUSKYLENS to learn one!"));
    // もし，ハスキーレンズがavailableでない場合（なんかエラー出たとき）
    else if(!huskylens.available()) Serial.println(F("No block or arrow appears on the screen!"));
    // 正常に動いた場合，ここが実際にどうしたいかっていうことを書いている
    else
    {
        Serial.println(F("###########"));
        while (huskylens.available())
        {
            HUSKYLENSResult result = huskylens.read();
            //　printResultは定義した関数，下に定義がある．
            controll_LED_output(result);
        }
    }
}

void controll_LED_output(HUSKYLENSResult result){
  if (result.command == COMMAND_RETURN_BLOCK){
    if(result.xCenter >= 160){
      digitalWrite( LED_PIN, HIGH );
      delay( 10 )
    } else{
      digitalWrite( LED_PIN, LOW );
      delay( 10 )
    }
  }
  else if (result.command == COMMAND_RETURN_ARROW){
    if(result.xOrigin >= 160){
      digitalWrite( LED_PIN, HIGH );
      delay( 10 )
    } else{
      digitalWrite( LED_PIN, LOW );
      delay( 10 )
    }
  }
}
